package po.steps;

import bo.AuthorizationPageBO;
import bo.GmailPageBO;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.WebDriver;
import utils.ConfigReader;
import utils.DriverManager;

import static org.testng.AssertJUnit.assertEquals;

public class MyStepdefs {

    private static WebDriver driver;
    private AuthorizationPageBO authorizationPageBO;
    private GmailPageBO gmailPageBO;

    public MyStepdefs() {
        authorizationPageBO = new AuthorizationPageBO();
        gmailPageBO = new GmailPageBO();
    }

    @Given("^User is on LogIn Gmail page$")
    public void userIsOnLogInGmailPage() {
        DriverManager.getDriver().get(ConfigReader.read("HOME_PAGE_OK"));
        driver = DriverManager.getDriver();
    }

    @When("^User enters \"([^\"]*)\" and \"([^\"]*)\"$")
    public void userEntersUserNameAndPassword(String userEmail, String password) {
        authorizationPageBO.enterLoginToGmailPage(userEmail);
        authorizationPageBO.enterPasswordToGmailPage(password);
    }

    @And("^sends Email$")
    public void sendsEmail() {
        gmailPageBO.sendEmail(ConfigReader.read("EMAIL_LOGIN"), ConfigReader.read("EMAIL_SUBJECT"), ConfigReader.read("EMAIL_MESSAGE"));
    }

    @And("^User delete last sent Email$")
    public void userDeleteLastSentEmail() {
        gmailPageBO.deleteLastEmail();
    }

    @Then("^User verifies whether last sent Email is deleted$")
    public void userVerifiesWhetherLastSentEmailIsDeleted() {
        assertEquals(gmailPageBO.getMessageFromDeleteLabel(), "Ланцюжок повідомлень переміщено в кошик.");
        DriverManager.closeResources();
    }


}

