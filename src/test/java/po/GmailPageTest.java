package po;

import bo.AuthorizationPageBO;
import bo.GmailPageBO;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.*;

import java.util.Iterator;
import java.util.Objects;
import java.util.stream.Stream;

import utils.CSVReader;
import utils.ConfigReader;
import utils.DriverManager;

import static org.testng.Assert.assertEquals;

public class GmailPageTest {

    private static WebDriver driver;
    private static WebDriverWait wait;

    private static final Logger LOG = LogManager.getLogger(GmailPageTest.class);

//    @DataProvider(name = "users", parallel = true)
//    public Iterator<Object[]> users() {
//        return Stream.of(
//                new Object[]{ConfigReader.read("EMAIL_LOGIN"), ConfigReader.read("EMAIL_PASSWORD")},
//                new Object[]{ConfigReader.read("SOME_EMAIL_LOGIN_2"), ConfigReader.read("SOME_EMAIL_PASSWORD_2")},
//                new Object[]{ConfigReader.read("SOME_EMAIL_LOGIN_3"), ConfigReader.read("SOME_EMAIL_PASSWORD_3")},
//                new Object[]{ConfigReader.read("SOME_EMAIL_LOGIN_4"), ConfigReader.read("SOME_EMAIL_PASSWORD_4")},
//                new Object[]{ConfigReader.read("SOME_EMAIL_LOGIN_5"), ConfigReader.read("SOME_EMAIL_PASSWORD_5")}).iterator();
//    }


    @DataProvider(name = "users", parallel = true)
    public Object[][] getUsersList() {
        return CSVReader.readInfoFromCSVFile("src/main/resources/users.csv", ",");
    }

    @BeforeMethod
    static void initializeObjects() {
        LOG.info("Open web-driver");
        DriverManager.getDriver().get(ConfigReader.read("HOME_PAGE"));
        LOG.info("Moving to login_submit page");
        driver = DriverManager.getDriver();
    }

    @Test(dataProvider = "users")
    public void openGmailAndLoginTest(String userEmail, String password) {
        AuthorizationPageBO authorizationPageBO = new AuthorizationPageBO();
        authorizationPageBO.enterLoginToGmailPage(userEmail);
        authorizationPageBO.enterPasswordToGmailPage(password);
        GmailPageBO gmailPageBO = new GmailPageBO();
        gmailPageBO.sendEmail(userEmail, ConfigReader.read("EMAIL_SUBJECT"), ConfigReader.read("EMAIL_MESSAGE"));
        gmailPageBO.deleteLastEmail();
        assertEquals(gmailPageBO.getMessageFromDeleteLabel(), "Ланцюжок повідомлень переміщено в кошик.");
    }

    @AfterMethod
    static void closeResources() {
        DriverManager.closeResources();
    }

}


