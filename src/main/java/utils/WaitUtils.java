package utils;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static po.BasicPage.driver;

public class WaitUtils {

    public static void waitElementToBeClickable(WebElement element) {
        WebDriverWait wait = new WebDriverWait(driver, Integer.parseInt(ConfigReader.read("DEFAULT_IMPLICITLY_WAIT_TIME")));
        wait.until(ExpectedConditions.elementToBeClickable(element));
    }
}
