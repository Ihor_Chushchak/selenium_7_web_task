package utils;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CSVReader {

    //static String line = "";

    public static String[][] readInfoFromCSVFile(String csvFile, String cvsSplitBy) {
        List<String[]> info = new ArrayList<>();
        String line = "";
        try (BufferedReader br = new BufferedReader(new FileReader(csvFile))) {
            while ((line = br.readLine()) != null) {
                info.add(line.split(cvsSplitBy));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return info.toArray(new String[info.size()][]);
    }

}


