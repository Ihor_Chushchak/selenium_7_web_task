package bo;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import po.GmailPage;

public class GmailPageBO {

    private static final Logger LOG = LogManager.getLogger(GmailPage.class);
    private GmailPage gmailPage;


    public GmailPageBO() {
        gmailPage = new GmailPage();
    }

    public void sendEmail(String to, String subject, String message) {
        gmailPage.clickComposeEmailButton();
        gmailPage.fillToField(to);
        gmailPage.fillSubjectField(subject);
        gmailPage.fillMessageField(message);
        gmailPage.clickSendEmailButton();
    }

    public void deleteLastEmail() {
        gmailPage.clickOpenMessageButton();
        if (gmailPage.checkEmailTimeSending() && gmailPage.checkEmailButton()) {
            gmailPage.clickDeleteEmailButton();
        } else {
            LOG.error("Message is not in sent directory");
        }
    }

    public String getMessageFromDeleteLabel() {
        return gmailPage.getDeleteProofMessage();
    }

}

