package decorator.elements;

import decorator.BasicElement;
import org.openqa.selenium.WebElement;
import utils.WaitUtils;

public class CustomLabel extends BasicElement {

    public CustomLabel(WebElement webElement) {
        super(webElement);
    }

    public String getText() {
        WaitUtils.waitElementToBeClickable(webElement);
        return super.getText();
    }
}