package decorator.elements;

import decorator.BasicElement;
import org.openqa.selenium.WebElement;
import po.BasicPage;
import utils.WaitUtils;

public class CustomButton extends BasicElement {

    public CustomButton(WebElement webElement) {
        super(webElement);
    }

    public void click() {
        WaitUtils.waitElementToBeClickable(webElement);
        super.click();
    }

}