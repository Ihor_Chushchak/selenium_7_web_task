package po;

import decorator.elements.CustomButton;
import decorator.elements.CustomTextInput;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.support.FindBy;

import org.openqa.selenium.support.ui.ExpectedConditions;

public class AuthorizationPage extends BasicPage {

    private static final Logger LOG = LogManager.getLogger(AuthorizationPage.class);

    @FindBy(css = "#identifierId")
    private CustomTextInput loginInput;

    @FindBy(xpath = "//div[@id='identifierNext']")
    private CustomButton loginNextButton;

    @FindBy(xpath = "//input[@type='password'][@name='password']")
    private CustomTextInput passwordInput;

    @FindBy(css = "#passwordNext")
    private CustomButton passwordNextButton;

    public void fillLogin(String login) {
        LOG.info("Entering login");
        loginInput.sendKeys(login);
    }

    public void clickLoginNextButton() {
        LOG.info("Moving to password_submit page");
        loginNextButton.click();
    }

    public void fillPassword(String password) {
        LOG.info("Entering password");
        passwordInput.sendKeys(password);
    }

    public void clickPasswordNextButton() {
        LOG.info("Moving to gmail_main page");
        passwordNextButton.click();
    }
}
